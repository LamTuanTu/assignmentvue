﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LowellBCTraining.Models.Article
{
    public class CategoryModel
    {
        public string Id { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
    }
}
