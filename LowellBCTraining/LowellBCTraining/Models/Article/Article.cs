﻿using LowellBCTraining.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LowellBCTraining.Models.Article
{
    public class Article
    {
        public string Id { get; set; }
        public string category { get; set; }
        public string slug { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string content { get; set; }
        public string image { get; set; }
        public DateTime createDate { get; set; }
        public UserModel CreateBy { get; set; } 
    }
}
