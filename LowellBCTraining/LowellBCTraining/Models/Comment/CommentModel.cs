﻿using LowellBCTraining.Models.Article;
using LowellBCTraining.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LowellBCTraining.Models.Comment
{
    public class CommentModel
    {
        public string content { get; set; }
        public string username { get; set; }
        public DateTime createdate { get; set; }
    }
}
