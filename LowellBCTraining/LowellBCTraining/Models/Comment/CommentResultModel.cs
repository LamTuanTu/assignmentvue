﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LowellBCTraining.Models.Comment
{
    public class CommentResultModel
    {
        public IList<ApplicationUser> User { get; set; }
        public IList<CommentModel> Comment { get; set; }
    }
}
