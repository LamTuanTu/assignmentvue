﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LowellBCTraining.Models.Comment
{
    public class CommentViewModel
    {
        public string content { get; set; }
        public string username { get; set; }
    }
}
