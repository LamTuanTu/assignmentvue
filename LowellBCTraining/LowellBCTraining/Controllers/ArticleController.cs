﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LowellBCTraining.Models;
using LowellBCTraining.Models.Article;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LowellBCTraining.Controllers
{
    [Produces("application/json")]
    [Route("api/Article")]
    public class ArticleController : Controller
    {
        
        [HttpPost]
        [Route("CreateArticle")]
        public IActionResult CreateArticle()
        {           
            var httpCurrent = HttpContext.Request.Form;
            if (httpCurrent == null)
            {
                return BadRequest();
            }            
            var username = httpCurrent["username"];
            var article = SetValueOfArticle(httpCurrent);
            if (article == null)
            {
                return BadRequest();
            }                
            Startup.GraphClient.Cypher
            .Create("(article:Article {article})")
            .WithParam("article", article)
            .ExecuteWithoutResults();

            Startup.GraphClient.Cypher
                .Match("(user:User)", "(article1:Article)")
                .Where($"user.UserName = '{username}'")
                .AndWhere($"article1.slug = '{article.slug}'")
                .Create("(article1)-[:AUTHOR]->(user)")
                .ExecuteWithoutResults();
                           
            return Ok();
        }

        [Route("GetArticles")]
        public IEnumerable<Article> GetArticles()
        {
            var data = Startup.GraphClient.Cypher
               .Match("(article:Article)")
               .Return((article) => new
               {
                   article = article.As<Article>()
               })
               .OrderBy("article.createDate DESC")
               .Results;
            var result = data.Select(t => t.article).ToList();
            return result;
        }

        [Route("{slug}")]
        public IActionResult GetArticle(string slug)
        {
            if (slug == null || slug == String.Empty)
            {
                return BadRequest();
            }
            var data = Startup.GraphClient.Cypher
                .Match("(article:Article)")
                .Where((Article article) => article.slug == slug)
                .Return((article) => new
                {
                    article = article.As<Article>()
                })
                .Results.FirstOrDefault().article;
            return Ok(data);
        }

        [Route("GetArticlesByUser/{username}")]
        public IEnumerable<Article> GetArticlesByUser(string username)
        {
            var data = Startup.GraphClient.Cypher
               .Match($"(user:User {{UserName: '{username}'}})<-[:AUTHOR]-(article)")
                .Return((article) => new
                {
                    article = article.As<Article>()
                })
               .OrderBy("article.createdate DESC")
               .Results;
            var result = data.Select(t => t.article).ToList();
            return result;
        }

        [Route("GetArticles/{category}")]
        public IEnumerable<Article> GetArticlesByCate(string category)
        {
            var data = Startup.GraphClient.Cypher
               .Match("(article:Article)")
               .Where((Article article) => article.category == category)
               .Return((article) => new
               {
                   article = article.As<Article>()
               })
               .OrderBy("article.createDate DESC")
               .Results;
            var result = data.Select(t => t.article).ToList();
            return result;
        }

        public Article SetValueOfArticle(IFormCollection httpCurrent)
        {
            var article = new Article();
            var file = httpCurrent.Files["file"];
            article.category = httpCurrent["category"];
            article.title = httpCurrent["title"];
            article.content = httpCurrent["content"];
            article.description = httpCurrent["description"];
            article.slug = GenerateSlug(article.title);
            string imageName = null;
            imageName = new String(Path.GetFileNameWithoutExtension(file.FileName).Take(10).ToArray());
            imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(file.FileName);
            var stream = new FileStream($"wwwroot/images/{imageName}", FileMode.Create);
            file.CopyTo(stream);
            article.image = "https://localhost:44349/images/" + imageName;
            article.createDate = DateTime.Now;
            return article;
        }

        public string GenerateSlug(string phrase)
        {
            string str = RemoveAccent(phrase).ToLower();
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            str = Regex.Replace(str, @"\s+", " ").Trim();
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-");  
            return str;
        }

        public string RemoveAccent(string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }
    }
}