﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LowellBCTraining.Models;
using LowellBCTraining.Models.Comment;
using LowellBCTraining.Models.User;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LowellBCTraining.Controllers
{
    [Produces("application/json")]
    [Route("api/Comment")]
    public class CommentController : Controller
    {
        [HttpPost]
        [Route("AddComment/{slug}")]
        public IActionResult AddComment([FromBody]CommentModel comment, string slug)
        {
            if (slug == String.Empty || slug == null || comment == null)
            {
                return BadRequest();
            }
            else if (comment.username == String.Empty)
            {
                ModelState.AddModelError("Login", "User not logged in");
                return BadRequest(ModelState);
            }
            else
            {
                Startup.GraphClient.Cypher
                .Match("(user:User), (article:Article)")
                .Where($"user.UserName = '{comment.username}'")
                .AndWhere($"article.slug = '{slug}'")
                .Create("(comment: Comment {comment})")
                .WithParam("comment", comment)
                .Create("(comment)-[:DISCUSS]->(article)")
                .Create("(comment)-[:OWNED]->(user)")
                .ExecuteWithoutResults();
            }            
            return Ok();
        }

        [Route("GetComments/{slug}")]
        public IEnumerable<CommentModel> GetComments(string slug)
        {
            var commentResultModel = new CommentModel();
            var data = Startup.GraphClient.Cypher
                .Match($"(article:Article {{slug: '{slug}'}})<-[:DISCUSS]-(comment)")
                .Return((comment) => new
                {
                    comment = comment.As<CommentModel>()
                })
               .OrderBy("comment.createdate DESC")
               .Results;
            var result = data.Select(t => t.comment).ToList();
            return result;
        }

        [Route("GetNumberOfComment")]
        public IActionResult GetNumberOfComment()
        {
            var number = Startup.GraphClient.Cypher
                        .Match("(comment:Comment)")
                        .Return((comment) => new
                        {
                            NumberOfComment = comment.Count()
                        })
                        .Results;
            var result = number.Select(t => t.NumberOfComment);
            return Ok(result);
                
        }
    }
}
