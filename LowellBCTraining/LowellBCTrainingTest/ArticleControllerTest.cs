﻿using LowellBCTraining.Controllers;
using LowellBCTraining.Models.Comment;
using Microsoft.AspNetCore.Mvc;
using System;
using Xunit;

namespace LowellBCTrainingTest
{
    public class ArticleControllerTest
    {
        private ArticleController _article = new ArticleController();

        [Fact]
        public void GetArticle_WithWrongParams_ReturnBadRequest()
        {
            string slugnull = null;
            string slugEmpty = String.Empty;
            var result1 = _article.GetArticle(slugnull);
            var result2 = _article.GetArticle(slugEmpty);
            Assert.IsType<BadRequestResult>(result1);
            Assert.IsType<BadRequestResult>(result2);
        }
    }
}
