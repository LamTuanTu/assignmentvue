using LowellBCTraining.Controllers;
using LowellBCTraining.Models.Comment;
using Microsoft.AspNetCore.Mvc;
using System;
using Xunit;

namespace LowellBCTrainingTest
{
    public class CommentControllerTest
    {
        private CommentController _comment = new CommentController();

        [Fact]
        public void AddComment_WithWrongParams_ReturnBadRequest()
        {
            string slugEmpty = String.Empty;
            string slugNull = null;
            var comment = new CommentModel();
            var result1 = _comment.AddComment(comment, slugEmpty);
            var result2 = _comment.AddComment(comment, slugNull);
            Assert.IsType<BadRequestResult>(result1);
            Assert.IsType<BadRequestResult>(result2);
        }

        [Fact]
        public void AddComment_WithEmptyUserName()
        {
            var comment = new CommentModel
            {
                content = "abc",
                username = String.Empty
            };
            string slug = "abc-xyz";
            var result = _comment.AddComment(comment, slug);
            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
