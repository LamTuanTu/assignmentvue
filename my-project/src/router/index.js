import Vue from 'vue'
import Router from 'vue-router'
import ProductList from '@/components/ProductList'
import Register from '@/components/Register'
import CreateProduct from '@/components/CreateProduct'
import ProductDetail from '@/components/ProductDetail'
import Login from '@/components/Login'

Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '',
      name: 'ProductList',
      component: ProductList,
      alias: ['/home']
    },
    {
      path: '/home/category/:category',
      name: 'ProductListByCate',
      component: ProductList
    },
    {
      path: '/home/article/:slug',
      name: 'ProductDetail',
      component: ProductDetail
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/create',
      name: 'CreateProduct',
      component: CreateProduct
    }
  ]
})
