import axios from 'axios'
/* eslint-disable */
export default {
    getArticles() {
        return axios.get('https://localhost:44349/api/Article/GetArticles');
    },
    getArticlesByCate(category) {
        return axios.get(`https://localhost:44349/api/Article/GetArticles/${category}`)
    },
    getArticleByUser(username) {
        return axios.post(`https://localhost:44349/api/Article/GetArticlesByUser/${username}`)
    },
    createArticle(data) {
        return axios.post('https://localhost:44349/api/Article/CreateArticle', data)
    },
    getArticle(slug) {
        return axios.get(`https://localhost:44349/api/Article/${slug}`)
    },
    register(data) {
        return axios.post('https://localhost:44349/Account/Register', data)
    },
    login(data) {
        return axios.post('https://localhost:44349/Account/Login', data)
    },
    createComment(data, slug) {
        return axios.post(`https://localhost:44349/api/Comment/AddComment/${slug}`, data)
    },
    getCommentBySlug(slug) {
        return axios.get(`https://localhost:44349/api/Comment/GetComments/${slug}`)
    },
    getNumberOfComment() {
        return axios.get('https://localhost:44349/api/Comment/GetNumberOfComment');
    }
}
